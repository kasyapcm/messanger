package org.kash.prac.messenger.exception;

public class DataNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2267340401949602915L;

	public DataNotFoundException(String message) {
		super(message);

}

}
