package org.kash.prac.messenger.model;

// member attribute of the message class so we do not assign XmlRootElement
public class Link {
	private String link;
	private String rel;
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getRel() {
		return rel;
	}
	public void setRel(String rel) {
		this.rel = rel;
	}
	

}
